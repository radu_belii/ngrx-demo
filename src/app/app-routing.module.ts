import { AuthenticatedGuardService } from './shared/guards/autenticated-guard.service';
import { NotFoundPageComponent } from '@app/core/containers/not-found-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  { path: '', redirectTo: '/books', pathMatch: 'full' },
  {
    path: 'books',
    loadChildren: '@app/modules/books/books.module#BooksModule',
  },
  {
    path: 'todo',
    canActivate: [AuthenticatedGuardService],
    loadChildren: '@app/modules/todo/todo.module#TodoModule',
  },
  {
    path: 'login',
    canActivate: [AuthenticatedGuardService],
    loadChildren: '@app/auth/auth.module#AuthModule',
  },
  { path: '**', component: NotFoundPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, })],
  exports: [RouterModule],
})

export class AppRoutingModule {}