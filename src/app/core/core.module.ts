import { HttpClientModule } from '@angular/common/http';
import { NotFoundPageComponent } from './containers/not-found-page.component';
import { MaterialModule } from '@app/material/index';
import { NgModule } from "@angular/core";
import { LayoutComponent } from './components/layout.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';

const COMPONENTS = [
  LayoutComponent,
  NotFoundPageComponent,
]

@NgModule({
  imports: [
    MaterialModule,
    CommonModule,
    RouterModule,
    HttpClientModule,
    EffectsModule.forRoot([])
  ],

  declarations: COMPONENTS,
  exports: COMPONENTS
})

export class CoreModule { }