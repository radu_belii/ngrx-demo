import { Component } from '@angular/core';

@Component({
  selector: 'bl-layout',
  template: `

  <mat-toolbar>
    <span>Book List app</span>
    <span class="spacer"></span>

    <mat-card-actions>
        <button mat-raised-button color="primary" routerLink='/todo'> Take Me to TODO </button>

      <button mat-raised-button color="primary" routerLink='/books'> Take Me to BOOKS </button>

      <button mat-raised-button color="primary" routerLink='/login'> Take Me to login</button>
    </mat-card-actions>
    
  </mat-toolbar>
  <main class="main-layout">
    <ng-content> </ng-content>
  </main>
        `,
  styles: [`
  .main-layout { max-width: 1120px; margin: 50px auto }
  .spacer { flex: 1 1 auto;}
  .mg-right-5 { margin-right: 5px}
  
  `]
})

export class LayoutComponent { }