import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Book } from '@app/modules/books/models/book';

@Injectable({
  providedIn: 'root',
})
export class GoogleBooksService {
  private API_PATH = 'https://www.googleapis.com/books/v1/volumes';

  constructor(private http: HttpClient) {}

  searchBooks(queryTitle: string): Observable<Book[]> {
    console.log('searchBooks', queryTitle);
    
    return this.http
      .get<{ items: Book[] }>(`${this.API_PATH}?q=${queryTitle}`)
      .pipe(map(books => books.items || []));
  }

  retrieveBook(volumeId: string): Observable<Book> {
    
    console.log(volumeId, '%volumeId',  'background: #222; color: #bada55 ') 
    return this.http.get<Book>(`${this.API_PATH}/${volumeId}`);
  }
}
