import { MaterialModule } from '@app/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginFormComponent } from '@app/auth/components/login-form.component';
MaterialModule

export const COMPONENTS = [
  LoginFormComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: COMPONENTS,
  entryComponents: [],
})
export class AuthModule {}
