import { Component } from '@angular/core';

@Component({
  selector:'todo-main-layout',
  template: `
  <div> 
  <p>Lorem</p>
    <todo-input-area> </todo-input-area>
    <todo-read> </todo-read>
  <div>`,
  styles: [`border: solid 1px #cccccc; padding: 10px`]
})

export class TodoMainLayout {}