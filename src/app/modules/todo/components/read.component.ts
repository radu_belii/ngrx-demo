import { Component, OnInit } from '@angular/core';
import { Todo } from '../models/todo.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'todo-read',
  template: `<p> Todos </p>
  <ul>
    <li *ngFor="let todo of todoList | async; let i = index">
      {{ todo.title }}
    </li>
  </ul>
  `,
  styles: [`background-color: #cccccc`]
})

export class ReadComponent implements OnInit {
  todoList: Observable<Todo[]>;

  constructor(private store: Store<AppState>) {
    this.todoList = store.select('todoState');
  }

  ngOnInit() {
  }

}