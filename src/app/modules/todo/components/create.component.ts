import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './../app.state';

import * as TodoActions from '../actions/todo.actions'
@Component({
  selector: 'todo-input-area',
  template: `
  <form class="todo-form">
    <mat-form-field class="full-width">
      <input #title matInput placeholder="Title of an todo">
    </mat-form-field>
  
    <mat-form-field class="full-width">
      <textarea matInput #desc placeholder="Leave a description"></textarea>
    </mat-form-field>
  </form>
  
  <button mat-button (click)="addNewTodo(title, desc)" >Add</button>
  `,
  styles: [`
    .todo-form { 
      border: 1px solid #cccccc;
      min-width: 150px;
      max-width: 500px;
      width: 100%;
    }

    .full-width {
      width: 100%
    }
    `]
})

export class TodoComponent {

  constructor(private store: Store<AppState>){}

  addNewTodo(title:HTMLInputElement, desc:HTMLTextAreaElement) {
    this.store.dispatch(new TodoActions.AddTodo({ title: title.value, description: desc.value }));

    // console.log(title.value, desc.value, 'data');
    
    title.value = '';
    desc.value ='';

  }
}