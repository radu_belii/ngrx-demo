import { Todo } from './models/todo.model';

export interface AppState {
  readonly todoState: Todo[]
}