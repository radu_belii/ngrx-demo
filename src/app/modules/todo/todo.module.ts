import { ReadComponent } from './components/read.component';
import { TodoMainLayout } from '@app/modules/todo/components/layout.component';
import { NgModule } from '@angular/core';
import { TodoComponent } from './components/create.component';
import { MaterialModule } from '@app/material';
import { TodoRoutingModule } from '@app/modules/todo/todo-routing.module';
import { CommonModule } from '@angular/common';

const COMPONENTS = [
  TodoComponent,
  ReadComponent,
  TodoMainLayout,
];

@NgModule({
  imports:[
    CommonModule,
    MaterialModule,
    TodoRoutingModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})

export class TodoModule {}