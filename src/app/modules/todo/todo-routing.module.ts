import { TodoMainLayout } from '@app/modules/todo/components/layout.component';
import { TodoComponent } from '@app/modules/todo/components/create.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  { path: '', component:  TodoMainLayout },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TodoRoutingModule {}
