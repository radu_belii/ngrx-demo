import { Todo } from './../models/todo.model';
import { Action } from '@ngrx/store';
import * as TodoActions from '../actions/todo.actions';

const initialTodo: Todo = {
    title: 'learn js',
    description: 'Lorem ipsum dolor asset init'
  }

export function reducer(state: Todo[] = [initialTodo], action: TodoActions.Actions) {
  switch(action.type){
    case TodoActions.ADD_TODO: 
      return [...state, action.payload];

    default:
      return state;
  }

}