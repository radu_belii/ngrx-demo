import { BookPreviewComponent } from './components/book-preview/book-preview.component';
import { BookEffects } from './effects/book-effects';
import { BooksRoutingModule } from './books-routing.module';
import { BookPreviewList } from './components/book-preview-list.component';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/material/material.module';
import { NgModule } from '@angular/core';
import { CollectionPageComponent } from '@app/modules/books/containers/collection-page.component';
import { EffectsModule } from '@ngrx/effects';

import { RouterModule } from '@angular/router';

const COMPONENTS = [
  BookPreviewList,
  CollectionPageComponent,
  BookPreviewComponent
]

@NgModule({
  imports:[
    CommonModule,
    MaterialModule,
    BooksRoutingModule,
    RouterModule,
    EffectsModule.forFeature([BookEffects]),
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})

export class BooksModule {}