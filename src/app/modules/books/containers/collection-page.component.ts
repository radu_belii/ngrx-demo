import { Component, OnInit, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'bl-collection-page',
  template: `
  <mat-card class="blue-bg">
      <mat-card-title>My Collection</mat-card-title>
  </mat-card>

  <book-preview *ngFor="let book of books | async; let i = index" [book]="book"></book-preview>
  `,
  styles: [`
    book-preview { display: inline-block }
    mat-card {
      background-color: rgb(33, 150, 243);
    }
    mat-card-title {
      display: flex;
      justify-content: center;
      color: #ffffff;
    }
    `,
  ]
})

// ng container!

export class CollectionPageComponent implements OnInit {
  books: Observable<any>;

  constructor(private store: Store<any>){
    this.books = store.select('booksState');

    store.select('booksState').subscribe( (value)=> console.log(value, '[---- value ------]'));
  }

  ngOnInit(){

  }
}