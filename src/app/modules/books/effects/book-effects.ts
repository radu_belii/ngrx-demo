import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as LoadCollectionActions  from '@app/modules/books/actions/load-collection-page.actions';
import { Book } from '@app/modules/books/models/book';
import { GoogleBooksService } from '@app/core/services/google-books.services'

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application.
 */

@Injectable()
export class BookEffects {
  constructor( private store: Store<any> , private googleBooks: GoogleBooksService ) {

    const pushToStore =  (books: Book[]) => {
      this.store.dispatch(new LoadCollectionActions.PopulateCollection(books));
    }

    this.googleBooks.searchBooks('Elon').subscribe((response) => pushToStore(response))
  } 
}