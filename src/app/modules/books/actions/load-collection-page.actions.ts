import { Book } from '@app/modules/books/models/book';
import { Action } from '@ngrx/store';

export const LOAD_COLLECTION = '[Collection Page] Load Collection';
export const LOAD_NEXT_COLLECTION = '[Collection Page] Load Next Collection';
export const POPULATE_COLLECTION = '[Collection Page] Populate Collection';

/**
 * Load Collection Action
 */
export class LoadCollection implements Action {
  readonly type = LOAD_COLLECTION;
}
export class LoadNextCollection implements Action {
  readonly type = LOAD_NEXT_COLLECTION;
}

export class PopulateCollection implements Action {
  readonly type = POPULATE_COLLECTION;
  
  constructor(public payload: Book[]){}
}
export type Actions = LoadCollection | LoadNextCollection | PopulateCollection;
