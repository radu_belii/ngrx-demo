import { Book } from '@app/modules/books/models/book';
import * as CollectionActions from '@app/modules/books/actions/load-collection-page.actions';

export interface State {
  readonly booksState: Book[]
}

const sampleBook:Book = {
  id: '000125',
  volumeInfo: {
    title: 'Sample Book',
    subtitle: 'subtitle sample',
    authors: ['John'],
    publisher: 'publisher',
    publishDate: 'publishDate',
    description: 'description',
    averageRating: 56452,
    ratingsCount: 453645,
    imageLinks: {
      thumbnail: 'thumbnail',
      smallThumbnail: 'smallThumbnail'
    }
  }
}

const initialState: Array<Book> =  [{ ...sampleBook}]


export function reducer(state = initialState, action: CollectionActions.Actions) {
  switch (action.type) {

    case CollectionActions.LOAD_COLLECTION: {
      return {
        ...state,
      };
    }
    case CollectionActions.POPULATE_COLLECTION: {
      return [...state, ...action.payload]
    }

    default: {
      return state;
    }
  }
}

