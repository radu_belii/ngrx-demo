import { AuthenticatedGuardService } from './guards/autenticated-guard.service';
import { NgModule } from '@angular/core';

import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';


const MODULES = [
  CommonModule,
  FormsModule,
  RouterModule,
  ReactiveFormsModule
];

const DIRECTIVES = [

];

@NgModule({
  imports: [
    ...MODULES
  ],
  exports: [
    ...MODULES
  ],
  declarations: [
    ...DIRECTIVES
  ]
})

export class SharedModule {}