import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import { log } from 'util';

@Injectable({
  providedIn: 'root'
})

export class AuthenticatedGuardService implements CanActivate {

  private isLoggedIn: boolean = true;

  constructor() {}

  canActivate() {
    console.info('[Can Activate]')
    return this.isLoggedIn
  }

}